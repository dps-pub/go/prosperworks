package prosperworks

import (
	"fmt"
)

type Email struct {
	Email    string `json:"email"`
	Category string `json:"category"`
}

type Social struct {
	URL      string `json:"url"`
	Category string `json:"category"`
}

// Person represents a person in Prosperworks
type Person struct {
	pw            *ProsperWorks
	ID            uint64        `json:"id,omitempty"`
	Name          string        `json:"name,omitempty"`
	Prefix        string        `json:"prefix,omitempty"`
	FirstName     string        `json:"first_name,omitempty"`
	MiddleName    string        `json:"middle_name,omitempty"`
	LastName      string        `json:"last_name,omitempty"`
	Suffix        string        `json:"suffix,omitempty"`
	Address       Address       `json:"address,omitempty"`
	AssigneeID    uint64        `json:"assignee_id,omitempty"`
	CompanyID     uint64        `json:"company_id,omitempty"`
	CompanyName   string        `json:"company_name,omitempty"`
	ContactTypeID uint64        `json:"contact_type_id,omitempty"`
	Details       string        `json:"details,omitempty"`
	Emails        []Email       `json:"emails,omitempty"`
	PhoneNumbers  []PhoneNumber `json:"phone_numbers,omitempty"`
	Socials       []Social      `json:"socials,omitempty"`
	Tags          []string      `json:"tags,omitempty"`
	Title         string        `json:"title,omitempty"`
	Websites      []Website     `json:"websites,omitempty"`
	CustomFields  []CustomField `json:"custom_fields,omitempty"`
	// DateCreated      time.Time     `json:"date_created,omitempty"`
	// DateModified     time.Time     `json:"date_modified,omitempty"`
	InteractionCount int64 `json:"interaction_count,omitempty"`
}

// GetPerson returns the person with ID
func (s *ProsperWorks) GetPerson(ID uint64) (*Person, error) {
	url := fmt.Sprintf("/people/%d", ID)
	var person Person

	err := s.request("GET", url, &person, nil)
	if err != nil {
		return nil, err
	}

	person.pw = s

	return &person, nil
}

// GetPeople returns a slice of People
func (s *ProsperWorks) GetPeople() ([]*Person, error) {
	url := fmt.Sprintf("/people/search")
	var people []*Person

	err := s.request("POST", url, &people, nil)
	if err != nil {
		return nil, err
	}

	for _, person := range people {
		person.pw = s
	}

	return people, nil
}

func (s *ProsperWorks) GetPersonActivities(ID uint64) ([]*Activity, error) {
	return s.getActivities(ID, "person")
}

func (m *Person) getActivities() ([]*Activity, error) {
	return m.pw.GetPersonActivities(m.ID)
}
