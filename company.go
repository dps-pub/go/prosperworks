package prosperworks

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
)

type FileUpload struct {
	AccessKeyID           string `json:"access_key_id"`
	Key                   string `json:"key"`
	Policy                string `json:"policy"`
	Signature             string `json:"signature"`
	SuccessActionRedirect string `json:"success_action_redirect"`
	URL                   string `json:"url"`
}

// Company represents a company in Prosperworks
type Company struct {
	pw *ProsperWorks

	ID               uint64        `json:"id"`
	Name             string        `json:"name"`
	Address          Address       `json:"address"`
	AssigneeID       interface{}   `json:"assignee_id"`
	ContactTypeID    interface{}   `json:"contact_type_id"`
	Details          string        `json:"details"`
	EmailDomain      string        `json:"email_domain"`
	PhoneNumbers     []PhoneNumber `json:"phone_numbers"`
	Socials          []interface{} `json:"socials"`
	Tags             []interface{} `json:"tags"`
	Websites         []Website     `json:"websites"`
	CustomFields     []CustomField `json:"custom_fields"`
	InteractionCount uint64        `json:"interaction_count"`
	DateCreated      uint64        `json:"date_created"`
	DateModified     uint64        `json:"date_modified"`
}

// GetCompany returns the company with ID
func (s *ProsperWorks) GetCompany(ID uint64) (*Company, error) {
	url := fmt.Sprintf("/companies/%d", ID)
	var company Company

	err := s.request("GET", url, &company, nil)
	if err != nil {
		return nil, err
	}

	company.pw = s

	return &company, nil
}

// GetCompanies returns a slice of companies
func (s *ProsperWorks) GetCompanies(query *Company) ([]*Company, error) {
	url := fmt.Sprintf("/companies/search")
	var companies []*Company

	b, err := json.Marshal(query)
	if err != nil {
		return nil, fmt.Errorf("couldn't marshal query: %v", err)
	}

	body := bytes.NewReader(b)

	err = s.request("POST", url, &companies, body)
	if err != nil {
		return nil, err
	}

	return companies, nil
}

func (s *ProsperWorks) UploadDocument(companyID uint64, body io.Reader, name string) error {
	url := fmt.Sprintf("https://app.prosperworks.com/api/v1/companies/165876/file_documents_api/s3_signed_url/?doc%%5dtitle%%5d=%s", name)
	var response FileUpload
	err := s.requestRaw("GET", url, response, body)
	if err != nil {
		return err
	}

	log.Println(response)

	return nil
}

func (m *Company) Update(body Company) error {
	url := fmt.Sprintf("/companies/%d", m.ID)

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(body)

	err := m.pw.request("PUT", url, m, b)
	if err != nil {
		return err
	}

	return nil
}

func (s *ProsperWorks) GetCompanyActivities(ID uint64) ([]*Activity, error) {
	return s.getActivities(ID, "company")
}

func (s *ProsperWorks) GetCompanyPeople(ID uint64) (res []Identifer, err error) {
	url := fmt.Sprintf("/companies/%d/related/people", ID)
	err = s.request("GET", url, &res, nil)
	return res, nil
}
