package prosperworks

import "fmt"

// Task represents a task in Prosperworks
type Task struct {
	ID              int           `json:"id"`
	Name            string        `json:"name"`
	RelatedResource Identifer     `json:"related_resource"`
	AssigneeID      int           `json:"assignee_id"`
	DueDate         int           `json:"due_date"`
	ReminderDate    interface{}   `json:"reminder_date"`
	CompletedDate   interface{}   `json:"completed_date"`
	Priority        string        `json:"priority"`
	Status          string        `json:"status"`
	Details         interface{}   `json:"details"`
	Tags            []interface{} `json:"tags"`
	CustomFields    []interface{} `json:"custom_fields"`
	DateCreated     int           `json:"date_created"`
	DateModified    int           `json:"date_modified"`
}

// GetTask returns the task with ID
func (s *ProsperWorks) GetTask(ID uint64) (*Task, error) {
	url := fmt.Sprintf("/tasks/%d", ID)
	var task Task

	err := s.request("GET", url, &task, nil)
	if err != nil {
		return nil, err
	}

	return &task, nil
}

// GetTasks returns a slice of task
func (s *ProsperWorks) GetTasks() ([]*Task, error) {
	url := fmt.Sprintf("/tasks/search")
	var tasks []*Task

	err := s.request("POST", url, &tasks, nil)
	if err != nil {
		return nil, err
	}

	return tasks, nil
}
