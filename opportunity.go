package prosperworks

import (
	"bytes"
	"encoding/json"
	"fmt"

	"github.com/shopspring/decimal"
)

type Opportunity struct {
	pw *ProsperWorks

	ID                 uint64          `json:"id,omitempty"`
	Name               string          `json:"name,omitempty"`
	AssigneeID         uint64          `json:"assignee_id,omitempty"`
	CloseDate          string          `json:"close_date,omitempty"`
	CompanyID          uint64          `json:"company_id,omitempty"`
	CompanyName        string          `json:"company_name,omitempty"`
	CustomerSourceID   uint64          `json:"customer_source_id,omitempty"`
	Details            string          `json:"details,omitempty"`
	LossReasonID       interface{}     `json:"loss_reason_id,omitempty"`
	PipelineID         uint64          `json:"pipeline_id,omitempty"`
	PipelineStageID    uint64          `json:"pipeline_stage_id,omitempty"`
	PrimaryContactID   uint64          `json:"primary_contact_id,omitempty"`
	Priority           string          `json:"priority,omitempty"`
	Status             string          `json:"status,omitempty"`
	Tags               []interface{}   `json:"tags,omitempty"`
	InteractionCount   int64           `json:"interaction_count,omitempty"`
	MonetaryUnit       interface{}     `json:"monetary_unit,omitempty"`
	MonetaryValue      decimal.Decimal `json:"monetary_value,omitempty"`
	WinProbability     int64           `json:"win_probability,omitempty"`
	DateLastContacted  interface{}     `json:"date_last_contacted,omitempty"`
	LeadsConvertedFrom []interface{}   `json:"leads_converted_from,omitempty"`
	DateLeadCreated    interface{}     `json:"date_lead_created,omitempty"`
	DateCreated        Time            `json:"date_created,omitempty"`
	DateModified       Time            `json:"date_modified,omitempty"`
	CustomFields       []CustomField   `json:"custom_fields,omitempty"`
}

func (m *Opportunity) AddActivity(text string) (*Activity, error) {
	return m.pw.AddActivity(m.ID, "opportunity", text)
}

func (m *Opportunity) Update(body Opportunity) error {
	url := fmt.Sprintf("/opportunities/%d", m.ID)

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(body)

	err := m.pw.request("PUT", url, m, b)
	if err != nil {
		return err
	}

	return nil
}

// GetOpportunity returns the Opportunity with ID
func (s *ProsperWorks) GetOpportunity(ID uint64) (*Opportunity, error) {
	url := fmt.Sprintf("/opportunities/%d", ID)

	var opportunity Opportunity
	err := s.request("GET", url, &opportunity, nil)
	if err != nil {
		return nil, err
	}
	opportunity.pw = s
	return &opportunity, nil
}

// GetOpportunity returns the Opportunity with ID
func (s *ProsperWorks) GetOpportunities() ([]*Opportunity, error) {
	url := fmt.Sprintf("/opportunities/search")

	var opportunities []*Opportunity
	err := s.request("POST", url, &opportunities, nil)
	if err != nil {
		return nil, err
	}

	return opportunities, nil
}

func (s *ProsperWorks) GetOpportunityActivities(ID uint64) ([]*Activity, error) {
	return s.getActivities(ID, "opportunity")
}
