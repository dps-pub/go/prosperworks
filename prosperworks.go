package prosperworks

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

type ErrorResponse struct {
	Success bool   `json:"success"`
	Status  int    `json:"status"`
	Message string `json:"message"`
}
type ProsperWorks struct {
	ApiKey   string
	ApiEmail string
}

type Website struct {
	URL      string `json:"url"`
	Category string `json:"category"`
}

type PhoneNumber struct {
	Number   string `json:"number"`
	Category string `json:"category"`
}

type Address struct {
	Street     string `json:"street"`
	City       string `json:"city"`
	State      string `json:"state"`
	PostalCode string `json:"postal_code"`
	Country    string `json:"country"`
}

func (s *ProsperWorks) requestRaw(method, url string, model interface{}, body io.Reader) error {
	// Build the request
	// logrus.Println(url)
	// logrus.Println(body)
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return err
	}

	req.Header.Set("content-type", "application/json")
	req.Header.Set("x-pw-accesstoken", s.ApiKey)
	req.Header.Set("x-pw-useremail", s.ApiEmail)
	req.Header.Set("x-pw-application", "developer_api")

	if err != nil {
		return fmt.Errorf("NewRequest: %s", err)
	}
	// For control over HTTP client headers,
	// redirect policy, and other settings,
	// create a Client
	// A Client is an HTTP client
	client := &http.Client{}

	// Send the request via a client
	// Do sends an HTTP request and
	// returns an HTTP response
	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Do: %s", err)
	}

	// Callers should close resp.Body
	// when done reading from it
	// Defer the closing of the body
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		var e ErrorResponse
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return fmt.Errorf("Decode: %s", err)
		}

		return errors.New(e.Message)
	}

	//
	// Use json.Decode for reading streams of JSON data
	if err := json.NewDecoder(res.Body).Decode(model); err != nil {
		return fmt.Errorf("Decode: %s", err)
	}

	return nil
}

func (s *ProsperWorks) Request(method, url string, model interface{}, body io.Reader) error {
	return s.request(method, url, model, body)
}

func (s *ProsperWorks) request(method, url string, model interface{}, body io.Reader) error {
	url = fmt.Sprintf("https://api.prosperworks.com/developer_api/v1%s", url)
	return s.requestRaw(method, url, model, body)
}
