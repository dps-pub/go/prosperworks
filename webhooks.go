package prosperworks

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
)

type WebhookType string
type WebhookEvent string

const (
	NewEvent    WebhookEvent = "new"
	UpdateEvent WebhookEvent = "update"
	DeleteEvent WebhookEvent = "delete"
)

const (
	LeadType        WebhookType = "lead"
	PersonType      WebhookType = "person"
	CompanyType     WebhookType = "company"
	OpportunityType WebhookType = "opportunity"
	ProjectType     WebhookType = "project"
	TaskType        WebhookType = "task"
)

type WebhookSecret struct {
	Secret string `json:"secret"`
	Key    string `json:"key"`
}

type Webhook struct {
	ID        int           `json:"id,omitempty"`
	Target    string        `json:"target"`
	Type      WebhookType   `json:"type"`
	Event     WebhookEvent  `json:"event"`
	Secret    WebhookSecret `json:"secret,omitempty"`
	CreatedAt int           `json:"created_at,omitempty"`
}

// GetWebhook returns the webhook with ID
func (s *ProsperWorks) GetWebhook(ID uint64) (*Webhook, error) {
	url := fmt.Sprintf("/webhooks/%d", ID)
	var webhook Webhook

	err := s.request("GET", url, &webhook, nil)
	if err != nil {
		return nil, err
	}

	return &webhook, nil
}

// GetWebhooks returns a slice of webhook
func (s *ProsperWorks) GetWebhooks() ([]*Webhook, error) {
	url := fmt.Sprintf("/webhooks")
	var webhooks []*Webhook

	err := s.request("GET", url, &webhooks, nil)
	if err != nil {
		return nil, err
	}

	return webhooks, nil
}

// Subscribe the url given to a event on a type of resource in prosperworks
func (s *ProsperWorks) Subscribe(url string, event WebhookEvent, eventType WebhookType) (*Webhook, error) {
	var webhook Webhook

	body := Webhook{
		Target: url,
		Event:  event,
		Type:   eventType,
		Secret: WebhookSecret{
			Secret: "test_secret",
			Key:    "test_key",
		},
	}

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(body)
	log.Println(b.String())
	err := s.request("POST", url, &webhook, b)
	if err != nil {
		return nil, err
	}

	return &webhook, nil
}
