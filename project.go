package prosperworks

import (
	"fmt"
)

// Project represents a project in Prosperworks
type Project struct {
	ID              int    `json:"id"`
	Name            string `json:"name"`
	RelatedResource struct {
		ID   int    `json:"id"`
		Type string `json:"type"`
	} `json:"related_resource"`
	AssigneeID   interface{}   `json:"assignee_id"`
	Status       string        `json:"status"`
	Details      string        `json:"details"`
	Tags         []interface{} `json:"tags"`
	CustomFields []interface{} `json:"custom_fields"`
	DateCreated  int           `json:"date_created"`
	DateModified int           `json:"date_modified"`
}

// GetProject returns the project with ID
func (s *ProsperWorks) GetProject(ID uint64) (*Project, error) {
	url := fmt.Sprintf("/projects/%d", ID)
	var project Project

	err := s.request("GET", url, &project, nil)
	if err != nil {
		return nil, err
	}

	return &project, nil
}

// GetProjects returns a slice of project
func (s *ProsperWorks) GetProjects() ([]*Project, error) {
	url := fmt.Sprintf("/projects/search")
	var projects []*Project

	err := s.request("POST", url, &projects, nil)
	if err != nil {
		return nil, err
	}

	return projects, nil
}
