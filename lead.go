package prosperworks

import (
	"fmt"
	"time"
)

// Lead represents a lead in Prosperworks
type Lead struct {
	ID         uint64      `json:"id"`
	Name       string      `json:"name"`
	Prefix     interface{} `json:"prefix"`
	FirstName  string      `json:"first_name"`
	LastName   string      `json:"last_name"`
	MiddleName interface{} `json:"middle_name"`
	Suffix     interface{} `json:"suffix"`
	Address    struct {
		Street     string `json:"street"`
		City       string `json:"city"`
		State      string `json:"state"`
		PostalCode string `json:"postal_code"`
		Country    string `json:"country"`
	} `json:"address"`
	AssigneeID       uint64 `json:"assignee_id"`
	CompanyName      string `json:"company_name"`
	CustomerSourceID uint64 `json:"customer_source_id"`
	Details          string `json:"details"`
	Email            struct {
		Email    string `json:"email"`
		Category string `json:"category"`
	} `json:"email"`
	MonetaryValue int64 `json:"monetary_value"`
	Socials       []struct {
		URL      string `json:"url"`
		Category string `json:"category"`
	} `json:"socials"`
	Status   string   `json:"status"`
	StatusID uint64   `json:"status_id"`
	Tags     []string `json:"tags"`
	Title    string   `json:"title"`
	Websites []struct {
		URL      string `json:"url"`
		Category string `json:"category"`
	} `json:"websites"`
	PhoneNumbers []struct {
		Number   string `json:"number"`
		Category string `json:"category"`
	} `json:"phone_numbers"`
	CustomFields []struct {
		CustomFieldDefinitionID uint64      `json:"custom_field_definition_id"`
		Value                   interface{} `json:"value"`
	} `json:"custom_fields"`
	DateCreated  time.Time `json:"date_created"`
	DateModified time.Time `json:"date_modified"`
}

// GetLead returns the lead with ID
func (s *ProsperWorks) GetLead(ID uint64) (*Lead, error) {
	url := fmt.Sprintf("/leads/%d", ID)
	var lead Lead

	err := s.request("GET", url, &lead, nil)
	if err != nil {
		return nil, err
	}

	return &lead, nil
}

// GetLeads returns a slice of lead
func (s *ProsperWorks) GetLeads() ([]*Lead, error) {
	url := fmt.Sprintf("/leads/search")
	var leads []*Lead

	err := s.request("POST", url, &leads, nil)
	if err != nil {
		return nil, err
	}

	return leads, nil
}
