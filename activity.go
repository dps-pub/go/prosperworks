package prosperworks

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
)

type Type struct {
	ID       uint64 `json:"id"`
	Category string `json:"category"`
}

var (
	Note Type = Type{
		ID:       0,
		Category: "user",
	}
)

type Activity struct {
	ID           uint64    `json:"id,omitempty"`
	UserID       uint64    `json:"user_id,omitempty"`
	Details      string    `json:"details,omitempty"`
	ActivityDate int64     `json:"activity_date,omitempty"`
	Parent       Identifer `json:"parent,omitempty"`
	Type         Type      `json:"type,omitempty"`
}

type ListActivities struct {
	Parent Identifer `json:"parent"`
}

// GetActivity returns the activity with ID
func (s *ProsperWorks) GetActivity(ID uint64) (*Activity, error) {
	url := fmt.Sprintf("/activities/%d", ID)
	var activity Activity

	err := s.request("GET", url, &activity, nil)
	if err != nil {
		return nil, err
	}

	return &activity, nil
}

// GetActivities returns a slice of activity
func (s *ProsperWorks) GetActivities() ([]*Activity, error) {
	url := fmt.Sprintf("/activities/search")
	var activities []*Activity

	err := s.request("POST", url, &activities, nil)
	if err != nil {
		return nil, err
	}

	return activities, nil
}

func (s *ProsperWorks) AddActivity(id uint64, parentType, text string) (*Activity, error) {
	url := fmt.Sprintf("/activities")
	body := Activity{
		Details: text,
		Parent: Identifer{
			ID:   id,
			Type: parentType,
		},
		Type: Type{
			Category: "user",
			ID:       332118,
		},
	}

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(body)

	log.Println(b)

	var activity *Activity
	err := s.request("POST", url, &activity, b)
	if err != nil {
		return nil, err
	}

	return activity, nil
}

func (s *ProsperWorks) getActivities(ID uint64, Type string) ([]*Activity, error) {
	url := fmt.Sprintf("/activities/search")
	body := ListActivities{
		Parent: Identifer{
			ID:   ID,
			Type: Type,
		},
	}
	b := new(bytes.Buffer)

	json.NewEncoder(b).Encode(body)

	var activities []*Activity
	err := s.request("POST", url, &activities, b)

	if err != nil {
		return nil, err
	}

	return activities, nil
}
