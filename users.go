package prosperworks

import "fmt"

// User represents a user in Prosperworks
type User struct {
	ID    uint64 `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

// GetUser returns the user with ID
func (s *ProsperWorks) GetUser(ID uint64) (*User, error) {
	url := fmt.Sprintf("/users/%d", ID)
	var user User

	err := s.request("GET", url, &user, nil)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetUsers returns a slice of User
func (s *ProsperWorks) GetUsers() ([]*User, error) {
	url := fmt.Sprintf("/users/search")
	var users []*User

	err := s.request("POST", url, &users, nil)
	if err != nil {
		return nil, err
	}

	return users, nil
}
