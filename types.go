package prosperworks

import "time"

type Identifer struct {
	ID   uint64 `json:"id"`
	Type string `json:"type"`
}

type Time struct {
	time.Time
}

type CustomField struct {
	ID    int         `json:"custom_field_definition_id"`
	Value interface{} `json:"value"`
}

// returns time.Now() no matter what!
func (t *Time) UnmarshalJSON(b []byte) error {
	// you can now parse b as thoroughly as you want

	*t = Time{time.Now()}
	return nil
}
